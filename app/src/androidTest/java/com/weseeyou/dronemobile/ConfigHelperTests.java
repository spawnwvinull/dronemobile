package com.weseeyou.dronemobile;

import android.content.Context;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ConfigHelperTests
{
    @Test
    public void getRemoteUrl()
    {
        // arrange
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        String expectedRemoteUrl = "http://192.168.178.29:5016/api/drone/register";

        // act
        String remoteUrl = ConfigHelper.getRemoteUrl(appContext);

        // assert
        assertEquals(expectedRemoteUrl, remoteUrl);
    }
}
