package com.weseeyou.dronemobile;

import android.content.Context;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class HostManagerTests
{
    @Test
    public void getHostname()
    {
        // arrange
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        HostManager hostManager = new HostManager(appContext);
        String expectedHostName = "Google Android SDK";
        // act
        String actualHostName = hostManager.getHostName();

        // assert
        assertTrue(actualHostName.startsWith(expectedHostName));
    }

    @Test
    public void getIpAddress()
    {
        // arrange
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        HostManager hostManager = new HostManager(appContext);
        String expectedIpAddress = "10.0.2.16";

        // act
        String actualHostName = hostManager.getIpAddress();

        // assert
        assertEquals(expectedIpAddress, actualHostName);
    }
}
