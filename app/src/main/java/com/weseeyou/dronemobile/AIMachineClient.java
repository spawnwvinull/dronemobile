package com.weseeyou.dronemobile;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

class AIMachineClient
{
    private String url;
    private WeakReference<Context> contextReference;

    AIMachineClient(String url, Context context)
    {
        this.url = url;
        this.contextReference = new WeakReference<>(context);
    }

    void register(String hostname, String ipAddress, String portNumber, String endpoint) throws JSONException
    {
        JSONObject json = new JSONObject();

        json.put("host_name", hostname);
        json.put("ip_address", ipAddress);
        String droneBaseAddress = "http://" + ipAddress + ":" + portNumber;
        json.put("drone_base_address", droneBaseAddress);
        json.put("drone_endpoint", endpoint);

        RequestQueue queue = Volley.newRequestQueue(this.contextReference.get());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, this.url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do nothing
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
            }
        });

        queue.add(jsonObjectRequest);
    }
}

