package com.weseeyou.dronemobile;

import android.content.Context;
import android.content.res.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class ConfigHelper
{
    static String getRemoteUrl(Context context)
    {
        String server = getConfigValue(context, "aimachine_address");
        String port = getConfigValue(context, "aimachine_port");
        String endpoint = getConfigValue(context, "aimachine_register_endpoint");
        return "http://" + server + ":" + port + endpoint;
    }

    private static String getConfigValue(Context context, String name) {
        Resources resources = context.getResources();

        try
        {
            InputStream rawResource = resources.openRawResource(R.raw.config);
            Properties properties = new Properties();
            properties.load(rawResource);
            return properties.getProperty(name);
        }
        catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
