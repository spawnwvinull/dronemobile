package com.weseeyou.dronemobile;

class ImageSaveError extends Exception
{
    ImageSaveError(String errorMessage, Throwable err)
    {
        super(errorMessage, err);
    }
}

class CurrentImagenotFound extends Exception
{
    CurrentImagenotFound(String errorMessage, Throwable err)
    {
        super(errorMessage, err);
    }
}

class CannotCreateDirectories extends Exception
{
    CannotCreateDirectories(String errorMessage, Throwable err)
    {
        super(errorMessage, err);
    }
}

class CannotDeleteFile extends Exception
{
    CannotDeleteFile(String errorMessage, Throwable err)
    {
        super(errorMessage, err);
    }
}
