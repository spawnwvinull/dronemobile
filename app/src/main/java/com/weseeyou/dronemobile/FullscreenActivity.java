package com.weseeyou.dronemobile;

import android.annotation.SuppressLint;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;

public class FullscreenActivity extends AppCompatActivity
{
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;

    private View mContentView;
    private boolean mVisible;

    // my components (no dependency injection)
    private ImageManager imageManager;
    private PermissionsManager permissionsManager;
    private ImageObserverManager imageObserverManager;
    private static boolean initialStartup = true;
    static String CURRENT_IMAGE_NAME = "currentimage.jpg";

    public FullscreenActivity()
    {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // swipe to refresh lister
        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.fullscreen_layout);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Intent refresh = new Intent(getApplicationContext(), FullscreenActivity.class);
                startActivity(refresh);
            }
        });

        // check permissions
        this.permissionsManager = this.setupPermissionsManager();
        this.permissionsManager.checkPermissions();

        // setup image manager
        try
        {
            this.imageManager = this.setupImageManager();
        }
        catch (CannotCreateDirectories cannotCreateDirectories)
        {
            cannotCreateDirectories.printStackTrace();
        }

        // set initial image
        if (initialStartup)
        {
            try
            {
                assert this.imageManager != null;
                Drawable initialImage = this.getResources().getDrawable(R.drawable.startup_image);
                this.imageManager.setupInitialImage(initialImage);
            }
            catch (ImageSaveError imageSaveError)
            {
                imageSaveError.printStackTrace();
            }
            initialStartup = false;
        }

        // put image on view
        try
        {
            this.setImageToView(imageManager);
        }
        catch (CurrentImagenotFound currentImagenotFound)
        {
            currentImagenotFound.printStackTrace();
        }

        // setup and start observer
        this.imageObserverManager = this.setupImageObserverManager();
        File fileToObserve = new File(this.imageManager.pathToStorage + File.separator, CURRENT_IMAGE_NAME);
        this.imageObserverManager.startObserving(fileToObserve);

        // start the http server
        HttpServer server = new HttpServer(imageManager);
        try
        {
            server.start();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // register with the AI Machine
        String url = ConfigHelper.getRemoteUrl(this);
        HostManager hostManager = new HostManager(this.getApplicationContext());
        String ipAddress = hostManager.getIpAddress();
        String hostName = hostManager.getHostName();
        AIMachineClient apiClient = new AIMachineClient(url, this.getApplicationContext());
        try
        {
            apiClient.register(hostName,  ipAddress, Integer.toString(HttpServer.PORT), server.RECEIVE_IMAGE_ENDPOINT);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private PermissionsManager setupPermissionsManager()
    {
        return new PermissionsManager(this);
    }

    private ImageManager setupImageManager() throws CannotCreateDirectories
    {
        String pathToStorage = this.getApplicationContext().getFilesDir().toString();
        return new ImageManager(pathToStorage);
    }

    private ImageObserverManager setupImageObserverManager()
    {
        return new ImageObserverManager(this);
    }


    private void setImageToView(ImageManager imageManager) throws CurrentImagenotFound
    {
        ImageView fullScreenImage = findViewById(R.id.fullscreen_content);
        Bitmap initialImage = imageManager.loadCurrentImage(CURRENT_IMAGE_NAME);
        fullScreenImage.setImageBitmap(initialImage);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };

    private final Handler mHideHandler = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
