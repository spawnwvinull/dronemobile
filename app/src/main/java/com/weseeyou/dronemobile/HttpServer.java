package com.weseeyou.dronemobile;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.HashMap;
import fi.iki.elonen.NanoHTTPD;

public class HttpServer extends NanoHTTPD
{
    final static int PORT = 8001;
    private ImageManager imageManager;

    final String RECEIVE_IMAGE_ENDPOINT = "/api/drone/image";
    private final String CHECK_SERVICE_ENDPOINT = "/api/drone/checkservice";
    private final String POSTDATA_KEY = "postData";
    private final String CURRENT_IMAGE_FILE_NAME = "currentimage.jpg";

    HttpServer(ImageManager imageManager)
    {
        super(PORT);
        this.imageManager = imageManager;
    }

    @Override
    public Response serve(IHTTPSession session)
    {
        String uri = session.getUri();
        String method = session.getMethod().name();
        switch (method)
        {
            case "POST":
                return handlePostRequests(session, uri);
            case "GET":
                return handleGetRequests(session, uri);
            default:
                return handledUnsupportedRequests(session, uri);
        }
    }

    private Response handleGetRequests(IHTTPSession session, String uri)
    {
        if (uri.equals(CHECK_SERVICE_ENDPOINT))
        {
            return checkService();
        }
        else
        {
            return handledUnsupportedRequests(session, uri);
        }
    }

    private Response checkService()
    {
        String message = "message: Service Available";
        return newFixedLengthResponse(message);
    }

    private Response handlePostRequests(IHTTPSession session, String uri)
    {
        try
        {
            HashMap<String, String> map = new HashMap<>();
            session.parseBody(map); // parse body into map
            String requestBody = map.get(POSTDATA_KEY);

            if (uri.equals(RECEIVE_IMAGE_ENDPOINT))
            {
                return receive_image(requestBody);
            }
            else
            {
                return handledUnsupportedRequests(session, uri);
            }
        }
        catch (IOException e)
        {
            return handledUnsupportedRequests(session, uri);
        }
        catch (ResponseException e)
        {
            return handledUnsupportedRequests(session, uri);
        }
        catch (JSONException e)
        {
            return handledUnsupportedRequests(session, uri);
        }
        catch (ImageSaveError imageSaveError)
        {
            return handledUnsupportedRequests(session, uri);
        }
    }

    private Response receive_image(String body) throws JSONException, ImageSaveError
    {
        JSONObject jsonObject = new JSONObject(body);
        String image = jsonObject.getString("image_data");

        this.imageManager.saveImage(image, CURRENT_IMAGE_FILE_NAME);

        return newFixedLengthResponse("Image received.");
    }

    private Response handledUnsupportedRequests(IHTTPSession session, String uri)
    {
        String method = session.getMethod().name();
        String message = "This method: " + method + " on this uri: " + uri + " is not supported";
        return newFixedLengthResponse(message);
    }
}
