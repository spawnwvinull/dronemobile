package com.weseeyou.dronemobile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class ImageManager
{
    String pathToStorage;
    private String currentImageName = "currentimage.jpg";

    ImageManager(String pathToStorage) throws CannotCreateDirectories
    {
        this.pathToStorage = pathToStorage;
        this.setupPaths(pathToStorage);
    }

    void saveImage(String imageAsBase64String, String fileName) throws ImageSaveError
    {

        File file = new File(this.pathToStorage + File.separator + fileName);
        Bitmap bmp = this.decode(imageAsBase64String);
        this.saveBitmap(file, bmp);
    }

    Bitmap loadCurrentImage(String fileName) throws CurrentImagenotFound
    {
        String currentFileName = this.pathToStorage + File.separator + fileName;
        File currentFile = new File(currentFileName);
        if (currentFile.exists())
        {
            return BitmapFactory.decodeFile(currentFileName);
        }

        throw new CurrentImagenotFound("Image not found: " + fileName, null);
    }

    void setupInitialImage(Drawable initialImageResource) throws ImageSaveError
    {
        Bitmap initialImage = this.drawableToBitmap(initialImageResource);
        File targetFileName = new File(this.pathToStorage + File.separator + currentImageName);
        this.saveBitmap(targetFileName, initialImage);
    }

    private void saveBitmap(File fileName, Bitmap bitmap) throws ImageSaveError
    {
        try
        {
            FileOutputStream out = new FileOutputStream(fileName);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch(IOException ex)
        {
            throw new ImageSaveError("Bitmap Image cannot be saved", ex);
        }
    }

    private void setupPaths(String pathToStorage) throws CannotCreateDirectories
    {
        File rootDir = new File(pathToStorage);
        if (!rootDir.exists())
        {
            if (!rootDir.mkdirs())
            {
                throw new CannotCreateDirectories("Cannot create the directories necessary", null);
            }
        }
    }

    private Bitmap decode(String imageAsBase64)
    {
        byte[] imageBytes = Base64.decode(imageAsBase64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable)
        {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;

            if (bitmapDrawable.getBitmap() != null)
            {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0)
        {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        }
        else
        {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}