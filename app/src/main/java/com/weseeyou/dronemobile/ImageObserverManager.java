package com.weseeyou.dronemobile;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.FileObserver;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Random;

class ImageObserverManager
{
    private Random randomGenerator = new Random();
    private WeakReference<Context> context;
    private String NOTIFICATION_CHANNEL_ID = "SOMEUNIQUEID1234";

    ImageObserverManager(Context context)
    {
        this.context = new WeakReference<>(context);
    }

    void startObserving(File fileToObserve)
    {
        this.registerNotificationChannel();
        FileObserver observer = this.setupObserver(fileToObserve);
        observer.startWatching();
    }

    private void registerNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence channelName = this.context.get().getString(R.string.notification_channel_name);
            String channelDescription = this.context.get().getString(R.string.notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(this.NOTIFICATION_CHANNEL_ID, channelName, importance);
            channel.setDescription(channelDescription);
            NotificationManager notificationManager = this.context.get().getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

    private FileObserver setupObserver(File fileToObserver)
    {
        final NotificationCompat.Builder notificationBuilder = this.setupNotificationBuilder();
        final int notificationId = this.createRandomNotificationId();
        final Activity activiyContext = (Activity)this.context.get();

        //noinspection deprecation
        return new FileObserver(fileToObserver.toString())
        {
            @Override
            public void onEvent(int event, @Nullable String path)
            {

                if (event == FileObserver.CREATE || event == FileObserver.MODIFY || event == FileObserver.ATTRIB)
                {
                    refreshActivity();

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(activiyContext);

                    notificationManager.notify(notificationId, notificationBuilder.build());
                }
            }
        };
    }

    private void refreshActivity()
    {
        Intent refresh = new Intent(this.context.get(), FullscreenActivity.class);
        this.context.get().startActivity(refresh);
    }

    private int createRandomNotificationId()
    {
        return this.randomGenerator.nextInt(1000000);
    }

    private NotificationCompat.Builder setupNotificationBuilder()
    {
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this.context.get(), FullscreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity((this.context.get()), 0, intent, 0);

        return new NotificationCompat.Builder(this.context.get(), this.NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_weseeyou_round_tranparent)
                .setContentTitle("We See You")
                .setContentText("New image created")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent);
    }
}
