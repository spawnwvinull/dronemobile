package com.weseeyou.dronemobile;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import java.lang.ref.WeakReference;

class PermissionsManager
{
    private WeakReference<Activity> context;
    PermissionsManager(Activity activity)
    {
        this.context = new WeakReference<>(activity);
    }
    void  checkPermissions()
    {
        this.checkWriteStoragePermissions();
        this.checkReadStoragePermissions();
    }

    private void checkWriteStoragePermissions()
    {
        if (ActivityCompat.checkSelfPermission(this.context.get(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.context.get(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    private void checkReadStoragePermissions()
    {
        if (ActivityCompat.checkSelfPermission(this.context.get(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.context.get(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }
}
